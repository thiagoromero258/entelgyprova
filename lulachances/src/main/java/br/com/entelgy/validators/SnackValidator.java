package br.com.entelgy.validators;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.entelgy.models.Ingridient;
import br.com.entelgy.models.Snack;

@Component
public class SnackValidator implements Validator {

	@Value("${snackingridientmandatoryids}")
	private String mandatoryIngridientTypes;

	@Value("${snackingridientrepeatmax}")
	private String fillingCanRepeat;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz == Snack.class;
	}

	@Override
	public void validate(Object target, Errors errors) {
		Snack snack = (Snack) target;
		Integer matchs = 0;
		String[] split = StringUtils.split(mandatoryIngridientTypes, ",");
		Integer hasToMatch = split.length;
		for (int i = 0; i < split.length; i++) {
			for (Ingridient ingridient : snack.getIngridients()) {
				if (ingridient.getIngridientType().getId().equals(Long.parseLong(split[i].trim()))) {
					matchs++;
				}
			}

		}

		if (hasToMatch != matchs) {
			errors.rejectValue("ingridients", "field.required");
		}

		Integer maxRepeated = Integer.parseInt(fillingCanRepeat);
		List<Ingridient> aux;
		for (Ingridient ingridient : snack.getIngridients()) {
			aux = snack.getIngridients().stream().filter(ingridient2 -> ingridient2.getId() == ingridient.getId())
					.collect(Collectors.toList());
			if (aux.size() > maxRepeated) {
				errors.rejectValue("ingridients", "field.required");
				break;
			}
			aux = null;
		}

	}
}
