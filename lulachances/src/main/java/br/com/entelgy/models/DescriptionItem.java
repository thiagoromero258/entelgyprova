package br.com.entelgy.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class DescriptionItem implements Serializable {

	private static final long serialVersionUID = 3949730989840316855L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String wordEnUs;

	private String wordPtBr;

	@OneToOne(mappedBy = "description")
	private Ingridient ingridient;

	@OneToOne(mappedBy = "type")
	private IngridientType ingridientType;

	@OneToOne(mappedBy = "description")
	private Snack snack;

	public DescriptionItem() {

	}

	public DescriptionItem(Long customItemId) {
		this.id = customItemId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWordEnUs() {
		return wordEnUs;
	}

	public void setWordEnUs(String wordEnUs) {
		this.wordEnUs = wordEnUs;
	}

	public String getWordPtBr() {
		return wordPtBr;
	}

	public void setWordPtBr(String wordPtBr) {
		this.wordPtBr = wordPtBr;
	}

	public Ingridient getIngridient() {
		return ingridient;
	}

	public void setIngridient(Ingridient ingridient) {
		this.ingridient = ingridient;
	}

	public IngridientType getIngridientType() {
		return ingridientType;
	}

	public void setIngridientType(IngridientType ingridientType) {
		this.ingridientType = ingridientType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((wordEnUs == null) ? 0 : wordEnUs.hashCode());
		result = prime * result + ((wordPtBr == null) ? 0 : wordPtBr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DescriptionItem other = (DescriptionItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (wordEnUs == null) {
			if (other.wordEnUs != null)
				return false;
		} else if (!wordEnUs.equals(other.wordEnUs))
			return false;
		if (wordPtBr == null) {
			if (other.wordPtBr != null)
				return false;
		} else if (!wordPtBr.equals(other.wordPtBr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Item Description id = " + getId() + " english = " + getWordEnUs();
	}

}
