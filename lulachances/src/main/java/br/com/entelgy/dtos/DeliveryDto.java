package br.com.entelgy.dtos;

import java.util.Calendar;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DeliveryDto {

	private Long id;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Calendar deliveryDate;

	private Integer deliveryNumber;

	private Boolean isDelivered;

	@NotNull
	@Size(max = 4)
	private List<InvoiceDto> invoices;

	public DeliveryDto() {

	}

	public DeliveryDto(List<InvoiceDto> invoices) {
		this.invoices = invoices;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Calendar deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Integer getDeliveryNumber() {
		return deliveryNumber;
	}

	public void setDeliveryNumber(Integer deliveryNumber) {
		this.deliveryNumber = deliveryNumber;
	}

	public Boolean getIsDelivered() {
		return isDelivered;
	}

	public void setIsDelivered(Boolean isDelivered) {
		this.isDelivered = isDelivered;
	}

	public List<InvoiceDto> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<InvoiceDto> invoices) {
		this.invoices = invoices;
	}
}
