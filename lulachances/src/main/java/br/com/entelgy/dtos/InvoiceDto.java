package br.com.entelgy.dtos;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class InvoiceDto {

	private Long id;

	@NotNull
	private List<SnackDto> snacks;

	@NotNull
	private PaymentDto paymentDto;

	private BigDecimal totalPrice;

	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Calendar createdOn;

	private Byte isDelivered;

	@NotNull
	private Boolean exchangeNeeded;

	@NotNull
	@Size(min = 10, max = 100)
	private String address;

	@NotNull
	@Size(min = 3, max = 75)
	private String requester;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public List<SnackDto> getSnacks() {
		return snacks;
	}

	public void setSnacks(List<SnackDto> snacks) {
		this.snacks = snacks;
	}

	public PaymentDto getPaymentDto() {
		return paymentDto;
	}

	public void setPaymentDto(PaymentDto paymentDto) {
		this.paymentDto = paymentDto;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Calendar getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Calendar createdOn) {
		this.createdOn = createdOn;
	}

	public Byte getIsDelivered() {
		return isDelivered;
	}

	public void setIsDelivered(Byte isDelivered) {
		this.isDelivered = isDelivered;
	}

	public Boolean getExchangeNeeded() {
		return exchangeNeeded;
	}

	public void setExchangeNeeded(Boolean exchangeNeeded) {
		this.exchangeNeeded = exchangeNeeded;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRequester() {
		return requester;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}
}
