package br.com.entelgy.dtos;

import java.math.BigDecimal;

public class CalculeDto {

	public CalculeDto(Integer numberOfPersons, BigDecimal valuePerPerson) {

		this.numberOfPersons = numberOfPersons;
		this.valuePerPerson = valuePerPerson;

	}

	private Integer numberOfPersons;

	private BigDecimal valuePerPerson;

	public Integer getNumberOfPersons() {
		return numberOfPersons;
	}

	public void setNumberOfPersons(Integer numberOfPersons) {
		this.numberOfPersons = numberOfPersons;
	}

	public BigDecimal getValuePerPerson() {
		return valuePerPerson;
	}

	public void setValuePerPerson(BigDecimal valuePerPerson) {
		this.valuePerPerson = valuePerPerson;
	}

}
