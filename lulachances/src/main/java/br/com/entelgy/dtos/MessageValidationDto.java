package br.com.entelgy.dtos;

public class MessageValidationDto extends MessageDto {
	private String field;

	public MessageValidationDto(MessageType type, String message, String field) {
		super(type, message);
		this.field = field;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

}