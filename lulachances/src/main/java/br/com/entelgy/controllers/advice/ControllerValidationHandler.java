package br.com.entelgy.controllers.advice;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.entelgy.dtos.MessageDto;
import br.com.entelgy.dtos.MessageType;
import br.com.entelgy.dtos.MessageValidationDto;
import br.com.entelgy.exceptions.InvalidValueException;

@ControllerAdvice
public class ControllerValidationHandler {
	@Autowired
	private MessageSource msgSource;

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public MessageValidationDto processValidationError(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		FieldError error = result.getFieldError();
		return processFieldError(error);
	}

	@ExceptionHandler(InvalidValueException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public MessageDto processValidationError(InvalidValueException ex) {
		ex.printStackTrace();
		Locale currentLocale = LocaleContextHolder.getLocale();
		String msg = msgSource.getMessage("field.invalid", null, currentLocale);
		return new MessageValidationDto(MessageType.ERROR, msg, ex.getField().getField());

	}

	private MessageValidationDto processFieldError(FieldError error) {
		MessageValidationDto message = null;
		if (error != null) {
			Locale currentLocale = LocaleContextHolder.getLocale();
			String msg = msgSource.getMessage("field.required", null, currentLocale);
			message = new MessageValidationDto(MessageType.ERROR, msg, error.getField());
		}
		return message;
	}
}