package br.com.entelgy.services;

import java.math.BigDecimal;
import java.util.List;

import br.com.entelgy.models.Invoice;

public interface InvoiceService {

	Invoice save(Invoice invoice);

	Invoice getById(Long id);

	List<Invoice> findAllUndelivered();

	BigDecimal calculateValueByPerson(Long invoiceId, Integer personQuantity);
}
