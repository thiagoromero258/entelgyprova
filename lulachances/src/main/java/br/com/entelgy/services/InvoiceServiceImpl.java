package br.com.entelgy.services;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import br.com.entelgy.models.Invoice;
import br.com.entelgy.models.UserSnacks;
import br.com.entelgy.repository.IngridientRepository;
import br.com.entelgy.repository.InvoiceRepository;
import br.com.entelgy.repository.SnackRepository;
import br.com.entelgy.repository.UserRepository;

@Component("invoiceService")
@Transactional
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public Invoice save(Invoice invoice) {
		UserSnacks customer = userRepository
				.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
		invoice.setCustomer(customer);
		invoice.setCreatedOn(Calendar.getInstance());
		return invoiceRepository.save(invoice);
	}

	@Override
	public Invoice getById(Long id) {
		return invoiceRepository.findOne(id);
	}

	@Override
	public List<Invoice> findAllUndelivered() {
		return invoiceRepository.findInvoicesUndelivered();
	}

	@Override
	public BigDecimal calculateValueByPerson(Long invoiceId, Integer personQuantity) {
		Invoice invoice = invoiceRepository.findOne(invoiceId);
		BigDecimal totalValue = invoice.calculePrice();

		return totalValue.divide(new BigDecimal(personQuantity), 2);

	}

}
