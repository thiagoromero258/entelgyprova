package br.com.entelgy.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;

import br.com.entelgy.exceptions.InvalidValueException;
import br.com.entelgy.models.DescriptionItem;
import br.com.entelgy.models.Snack;
import br.com.entelgy.repository.SnackRepository;
import br.com.entelgy.validators.SnackValidator;

@Component("snackService")
@Transactional
public class SnackServiceImpl implements SnackService {

	@Autowired
	private SnackRepository snackRepository;

	@Value("${snackcustomid}")
	private String customId;

	@Autowired
	private SnackValidator validator;

	@Override
	public List<Snack> findAll() {
		return (List<Snack>) snackRepository.findAll();
	}

	@Override
	public Snack findById(Long id) {
		return snackRepository.findOne(id);
	}

	@Override
	public Snack save(Snack snack) throws InvalidValueException {
		if (snack.getDescription() == null) {
			snack.setDescription(new DescriptionItem(Long.parseLong(customId)));
		}
		DataBinder binder = new DataBinder(snack);
		binder.setValidator(validator);
		binder.validate();
		if (binder.getBindingResult().hasErrors()) {
			throw new InvalidValueException(binder.getBindingResult().getFieldError());
		}

		return snackRepository.save(snack);
	}
}
