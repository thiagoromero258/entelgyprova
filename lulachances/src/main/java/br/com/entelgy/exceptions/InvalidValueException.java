package br.com.entelgy.exceptions;

import org.springframework.validation.FieldError;

public class InvalidValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5416393166543531281L;
	private FieldError field;

	public InvalidValueException(FieldError fieldError) {
		super();
		this.field = fieldError;
	}

	public FieldError getField() {
		return field;
	}

}
