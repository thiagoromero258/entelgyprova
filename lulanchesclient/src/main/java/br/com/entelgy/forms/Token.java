package br.com.entelgy.forms;

public class Token {
	
	public String token;
	public String role;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "Token [token=" + token + ", role=" + role + "]";
	}
	
	
}
