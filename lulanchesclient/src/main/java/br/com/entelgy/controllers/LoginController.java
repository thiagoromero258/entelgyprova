package br.com.entelgy.controllers;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import br.com.entelgy.forms.LoginForm;
import br.com.entelgy.forms.Token;

@Controller
@RequestMapping(value = "login")
public class LoginController {

	@Autowired
	RestTemplate restTemplate;
	
	@RequestMapping(method = RequestMethod.GET)
	public String login(){
		
		return "login";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String login(@Valid LoginForm form , BindingResult bindingResult) {
		
		if (bindingResult.hasErrors()){
			return "login";
		}
		
		String url = "http://localhost:8090/auth";
		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type", "application/json");
		
		HttpEntity<LoginForm> json2 = new HttpEntity<LoginForm>(form ,headers);
		ResponseEntity<Token> responseEntity = restTemplate.postForEntity(url, json2, Token.class);
		
		Token token = responseEntity.getBody();
		
		if (token.getRole().indexOf("ADMIN") > 0){
			return "management";
		}else{
			return "order";
		}

	}
}
